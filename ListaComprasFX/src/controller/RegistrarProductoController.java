package controller;

import bsn.ProductoBsn;
import bsn.exception.ProductoExistenteException;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import model.Producto;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;

import java.util.List;

public class RegistrarProductoController {


    private List<Producto> productoList;

    @FXML
    private TableView tblProductos;
    @FXML
    private TextField txtProducto;
    @FXML
    private TextField txtCandtidad;


    @FXML
    private TableColumn<Producto, String> clmNombre;
    @FXML
    private TableColumn<Producto, String> clmCantidad;


    private ProductoBsn productoBsn;

    public RegistrarProductoController() {
        this.productoBsn = new ProductoBsn();
    }

    private void refrescarTabla() {
        List<Producto> productoList = productoBsn.consultarProductos();
        ObservableList<Producto> productoObservableList = FXCollections.observableList(productoList);
        tblProductos.setItems(productoObservableList);
    }

    @FXML
    public void initialize() {
        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombreProducto()));
        clmCantidad.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCantidadProducto()));
        List<Producto> productoList = productoBsn.consultarProductos();
        ObservableList<Producto> productoObservableList = FXCollections.observableList(productoList);
        tblProductos.setItems(productoObservableList);

    }

    public void btnRegistrar_action() {



        String productoIngresado = txtProducto.getText().trim();
        String cantidadIngresada = txtCandtidad.getText().trim();

        if (productoIngresado.isEmpty()) {
            mensajeError(Alert.AlertType.ERROR, "Registro de producto", "Error al procesar", "El nombre del producto es necesario");
            txtProducto.requestFocus();
            return;
        }

        if (cantidadIngresada.isEmpty()) {
            mensajeError(Alert.AlertType.ERROR, "Registro de producto", "Error al procesar", "La cantidad del producto es necesaria");
            txtCandtidad.requestFocus();
            return;
        }
        try {
            Integer.parseInt(cantidadIngresada);
        } catch (NumberFormatException numberFormatException) {
            mensajeError(Alert.AlertType.ERROR, "Registro de producto", "Error al procesar", "La cantidad debe ser un valor numerico");
            txtCandtidad.requestFocus();
            txtCandtidad.clear();
            return;
        }

        Producto producto = new Producto(productoIngresado, cantidadIngresada);
        try {
            this.productoBsn.ingresarProducto(producto);
            mensajeError(Alert.AlertType.INFORMATION, "Registro de Producto", "Realizado con exito", "Producto registrado");
            limpiar();
        } catch (ProductoExistenteException e) {
            mensajeError(Alert.AlertType.ERROR, "Registro de producto", "Error al procesar", e.getMessage());
        }

    }

    private void mensajeError(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    private void limpiar() {
        this.txtCandtidad.clear();
        this.txtProducto.clear();
    }

    private boolean validarCampos(String... campos) {
        boolean correcto = true;
        for (String campo : campos) {
            if (campo == null || campo.trim().isEmpty()) {
                correcto = false;
                break;
            }
        }
        return correcto;
    }
}
