package dao.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import static java.nio.file.StandardOpenOption.APPEND;


import model.Producto;
import dao.ProductoDAO;


import java.util.List;
import java.util.Optional;

public class ProductoDAONIO implements ProductoDAO {

    private final static String NOMBRE_ARCHIVO = "src/documents/productos.txt";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final static Integer LONG_NOMBRE = 50;
    private final static Integer LONG_CANTIDAD = 50;
    private final static Integer LONG_REGISTRO = 50;

    public static List<Producto> productoList = new ArrayList<>();


    public ProductoDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarProducto(Producto producto) {
        String registro = parseProudctoString(producto);
        byte[] datosRegistro = registro.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseProudctoString(Producto producto) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampo(producto.getCantidadProducto(), LONG_CANTIDAD))
                .append(completarCampo(producto.getNombreProducto(), LONG_NOMBRE));
        return sb.toString();
    }

    private String completarCampo(String valor, Integer longitudcampo) {
        if (valor.length() > longitudcampo) {
            return valor.substring(0, longitudcampo);
        }
        return String.format("%1$" + longitudcampo + "s", valor);
    }

    @Override
    public List<Producto> consultarProductos() {
        refrescarTabla();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONG_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Producto producto = parseBufferProducto(registro);
                productoList.add(producto);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();

        }
        return productoList;
    }

    @Override
    public void refrescarTabla() {
        this.productoList = new ArrayList<>();
    }

    @Override
    public Optional<Producto> buscarNombre(String nombre) {
        for (Producto producto : productoList) {
            if (producto.getNombreProducto().equals(nombre)) {
                return Optional.of(producto);
            }
        }
        return Optional.empty();
    }

    private Producto parseBufferProducto(CharBuffer registro) {
        Producto producto = new Producto();

        String cantidad = registro.subSequence(0, LONG_CANTIDAD).toString().trim();
        producto.setCantidadProducto(cantidad);
        registro.position(LONG_CANTIDAD);


        String name = registro.subSequence(0, LONG_NOMBRE).toString().trim();
        producto.setNombreProducto(name);
        registro.position(LONG_NOMBRE);
        registro = registro.slice();
        return producto;
    }


}
