package dao;

import model.Producto;

import java.util.List;
import java.util.Optional;

public interface ProductoDAO {
    void registrarProducto(Producto producto);

    Optional<Producto> buscarNombre(String nombre);

    List<Producto> consultarProductos();

    void refrescarTabla();


}
