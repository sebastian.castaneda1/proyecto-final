package bsn;

import bsn.exception.ProductoExistenteException;
import dao.ProductoDAO;
import dao.impl.ProductoDAONIO;
import model.Producto;

import java.util.List;
import java.util.Optional;

public class ProductoBsn {
    private ProductoDAO productoDAO;

    public ProductoBsn() {
        this.productoDAO = new ProductoDAONIO();
    }

    public void ingresarProducto(Producto producto) throws ProductoExistenteException {
        Optional<Producto> productoOptional = this.productoDAO.buscarNombre(producto.getNombreProducto());
        if (productoOptional.isPresent()) {
            throw new ProductoExistenteException();
        } else {
            this.productoDAO.registrarProducto(producto);
        }
    }
    public boolean consultarProducto(Producto producto)  {
        Optional<Producto> productoOptional = this.productoDAO.buscarNombre(producto.getNombreProducto());
        if (productoOptional.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    public List<Producto> consultarProductos() { return productoDAO.consultarProductos(); }

}
