package bsn.exception;

public class ProductoExistenteException extends Exception {
    public ProductoExistenteException() {
        super
                ("El producto ingresado ya existe");
    }
}
